import javax.swing.*;
import java.awt.*;

/**
  Created by Emilia on 2020-04-24.
 */
public class GraKIK extends JFrame {

    private static int SZEROKOSC_ZNAKU = 45;
    private static int PANEL_SZE = SZEROKOSC_ZNAKU * 3;
    private static int PANEL_WYS = SZEROKOSC_ZNAKU * 3;
    private static int RAMKA_SZE = PANEL_SZE + 135;
    private static int RAMKA_WYS = PANEL_WYS + 60;

    private static final Color K_PLANSZY = new Color(37, 149, 183);
    private static final Color K_TLO = new Color(46, 99, 129);

    private JPanel panel;
    private PoleGry poleGry;


    private GraKIK() {
        super("Gra w Kółko i Krzyżyk");

        panel = new JPanel();
        poleGry = new PoleGry();

        poleGry.setBounds(10, 10, PANEL_SZE, PANEL_WYS);
        poleGry.setBackground(K_PLANSZY);
        panel.setLayout(null);
        panel.setBackground(K_TLO);
        panel.add(poleGry);
        add(panel);



        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(RAMKA_SZE, RAMKA_WYS);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
    }

    public static void main(String[] args) {
        new GraKIK();
    }
}